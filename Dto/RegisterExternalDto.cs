﻿using System.ComponentModel.DataAnnotations;

namespace MFA_Blog.Dto
{
    public class RegisterExternalDto
    {
        [Required(ErrorMessage = "Have to supply a name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Have to supply an e-mail address")]
        public string Email { get; set; }
    }
}
