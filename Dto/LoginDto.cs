﻿namespace MFA_Blog.Dto
{
    public class LoginDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string RequestPath { get; set; }
        public bool RememberMe { get; set; }
    }
}
