﻿namespace MFA_Blog.Dto
{
    public class UserDto
    {
        private int _userId;
        private string _name;
        private string _surname;
        private string _email;
        private string _password;

        public int UserId { get => _userId; set => _userId = value; }
        public string Name { get => _name; set => _name = value; }
        public string Surname { get => _surname; set => _surname = value; }
        public string Email { get => _email; set => _email = value; }
    }
}
