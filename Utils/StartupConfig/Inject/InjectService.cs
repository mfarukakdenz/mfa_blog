﻿using MFA_Blog.Service;
using MFA_Blog.Service.Imp;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MFA_Blog.Utils.StartupConfig.Inject
{
    public class InjectService
    {
        public static void InjectServiceMethod(IServiceCollection services)
        {
            services.AddScoped<IUserService, UserServiceImp>();
        }
    }
}
