﻿using Microsoft.Extensions.DependencyInjection;

namespace MFA_Blog.Utils.StartupConfig.Inject.Global
{
    public class InjectGlobal
    {
        public static void InjectGlobalMethod(IServiceCollection services)
        {
            InjectService.InjectServiceMethod(services);
            InjectDao.InjectDaoMethod(services);
        }
    }
}
