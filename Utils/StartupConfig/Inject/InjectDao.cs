﻿using MFA_Blog.Dao;
using MFA_Blog.Dao.Imp;
using Microsoft.Extensions.DependencyInjection;

namespace MFA_Blog.Utils.StartupConfig.Inject
{
    public class InjectDao
    {
        public static void InjectDaoMethod(IServiceCollection services)
        {
            services.AddScoped<IUserDao, UserDaoImp>();
        }
    }
}
