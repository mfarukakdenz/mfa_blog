﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MFA_Blog.Utils.Db
{
    public class DbInitialize
    {
        public static void Initialize(ApplicationDbContext appContext)
        {
            appContext.Database.EnsureCreated();
            if (appContext.User.Any())
                return;

            appContext.User.Add(new Models.UserModel()
            {
                Name = "Faruk",
                Surname = "Akdeniz",
                Password = "123123"
            });

            appContext.SaveChanges();
        }
    }
}
