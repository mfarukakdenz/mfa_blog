﻿using MFA_Blog.Models;
using Microsoft.EntityFrameworkCore;

namespace MFA_Blog.Utils.Db
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> option) : base(option) { }
        public DbSet<UserModel> User { get; set; }
    }
}
