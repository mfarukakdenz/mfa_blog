﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MFA_Blog.Controllers
{
    [Route("admin")]
    public class PostController : Controller
    {
        [HttpGet("post/list")]
        public IActionResult PostList()
        {
            return View();
        }

        [HttpGet("post/add")]
        public IActionResult PostAdd()
        {
            return View("AddOrUpdatePost");
        }

        [HttpGet("post/update")]
        public IActionResult PostUpdate(int postId)
        {
            return View("AddOrUpdatePost");
        }
    }
}