﻿using MFA_Blog.Dto;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MFA_Blog.Controllers
{
    [Route("auth")]
    public class AuthController : Controller
    {
        [HttpGet("login")]
        public IActionResult Login(string requestPath)
        {
            ViewBag.RequestPath = requestPath ?? "/";
            return View();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginDto loginDto)
        {
            return await SignInUser(loginDto);
        }

        [Route("logout")]
        public async Task<IActionResult> Logout(string requestPath)
        {
            await HttpContext.SignOutAsync(scheme: "MFASecurityScheme");

            return RedirectToAction("Login");
        }


        [Route("loginexternal/{id}")]
        public Task LogInExternal(string id)
        {
            return HttpContext.ChallengeAsync(id, new AuthenticationProperties { RedirectUri = "/auth/registerexternal" });
        }

        [Route("registerexternal")]
        public async Task<IActionResult> RegisterExternal(string authprovider)
        {
            var authResult = await HttpContext.AuthenticateAsync("TempCookie");
            if (!authResult.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }

            var facebookUserClaims = authResult.Principal.Claims;
            await HttpContext.SignInAsync(authResult.Principal);
            //Bu kısımda bilgileri modele ekleyip register sayfasına yönlendirip ilgili alanları doldurup kişinin kaydolmasını sağlayabiliriz.
            return Redirect("/");
        }

        [Route("registerexternal/{id}")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> RegisterExternal(string id, RegisterDto registerDto)
        {
            var authResult = await HttpContext.AuthenticateAsync("TempCookie");
            if (!authResult.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View(registerDto);
            }

            //Veri tabanına kaydet ve Login et
            //var user = await userService.AddExternal(authResult.Principal.FindFirstValue(ClaimTypes.NameIdentifier), registerDto.Name, registerDto.Email);
            LoginDto loginDto = new LoginDto()
            {
                Username = registerDto.Email,
            };


            return await SignInExternal(loginDto);
        }

        private async Task<IActionResult> SignInExternal(LoginDto loginDto)
        {
            await HttpContext.SignOutAsync("TempCookie");
            return await SignInUser(loginDto);
        }

        private async Task<IActionResult> SignInUser(LoginDto loginDto)
        {

            if (!IsAuthentic(loginDto.Username, loginDto.Password))
                return View();

            // create claims
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, loginDto.Username),
                new Claim(ClaimTypes.Email, loginDto.Username)
            };

            // create identity
            ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

            // create principal
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            // sign-in
            await HttpContext.SignInAsync(
                    scheme: "MFASecurityScheme",
                    principal: principal,
                    properties: new AuthenticationProperties
                    {
                        IsPersistent = loginDto.RememberMe, // for 'remember me' feature
                        ExpiresUtc = DateTime.UtcNow.AddMinutes(20)
                    });

            return Redirect(loginDto.RequestPath ?? "/");
        }

        public IActionResult Access()
        {
            return View();
        }


        private bool IsAuthentic(string username, string password)
        {
            return (username == "nurullah.yayan@gmail.com" && password == "123123");
        }
    }
}