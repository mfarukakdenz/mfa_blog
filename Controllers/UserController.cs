﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFA_Blog.Dto;
using MFA_Blog.Service;
using Microsoft.AspNetCore.Mvc;

namespace MFA_Blog.Controllers
{
    [Route("admin")]
    public class UserController : Controller
    {
        IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("user/list")]
        public IActionResult UserList()
        {
            List<UserDto> userDtos = _userService.GetUsers();

            return View(userDtos);
        }

        [HttpGet("user/add")]
        public IActionResult AddUser()
        {
            return View("AddOrUpdateUser");
        }

        [HttpPost("user/add")]
        public IActionResult AddUser(UserDto user, string password)
        {
            if (user.UserId > 0)
            {
                _userService.UpdateUser(user,password);
            }
            else
            {
                _userService.AddUser(user, password);
            }


            return View("AddOrUpdateUser", user);
        }

        [HttpGet("user/edit/{userId}")]
        public IActionResult EditUser(int userId)
        {
            UserDto userDto = _userService.GetUserById(userId);

            return View("AddOrUpdateUser", userDto);
        }
    }
}