﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFA_Blog.Models;
using MFA_Blog.Utils.Db;

namespace MFA_Blog.Dao.Imp
{
    public class UserDaoImp : IUserDao
    {
        ApplicationDbContext _context;

        public UserDaoImp(ApplicationDbContext context)
        {
            _context = context;
        }

        public UserModel AddUser(UserModel user)
        {
            //user.Password = password; Örnek
            _context.User.Add(user); //Database kaydedilecek son hali context içeriğine eklenir. Yapılacak işlemler varsa öncesinde yapılmalıdır.
            _context.SaveChanges(); //Database kaydetme işlemi gerçekleştirilir.

            return user;
        }

        public UserModel GetUserById(int userId)
        {
            return _context.User.Find(userId);
        }

        public List<UserModel> GetUsers()
        {
            return _context.User.ToList();
        }

        public UserModel UpdateUser(UserModel user)
        {
            _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges(); //Database kaydetme işlemi gerçekleştirilir.

            return user;
        }
    }
}
