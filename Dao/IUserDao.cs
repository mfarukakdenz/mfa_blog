﻿using MFA_Blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MFA_Blog.Dao
{
    public interface IUserDao
    {
        UserModel AddUser(UserModel user); // UserDto -> Geri Dönüş Değeri, addUser -> Metot İsmi, (UserDto user -> Beklediği Sınıf);
        UserModel UpdateUser(UserModel user); 
        UserModel GetUserById(int userId); 
        List<UserModel> GetUsers();
    }
}
