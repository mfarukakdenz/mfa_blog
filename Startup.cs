﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MFA_Blog.Utils.Db;
using MFA_Blog.Utils.StartupConfig.Inject.Global;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MFA_Blog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAuthentication("MFASecurityScheme")
                   .AddCookie("MFASecurityScheme", options =>
                   {
                       options.AccessDeniedPath = new PathString("/Security/Access");
                       options.Cookie = new CookieBuilder
                       {
                           //Domain = "",
                           HttpOnly = true,
                           Name = ".MFA.Security.Cookie",
                           Path = "/",
                           SameSite = SameSiteMode.Lax,
                           SecurePolicy = CookieSecurePolicy.SameAsRequest
                       };
                       options.Events = new CookieAuthenticationEvents
                       {
                           OnSignedIn = context =>
                           {
                               Console.WriteLine("{0} - {1}: {2}", DateTime.Now,
                                 "OnSignedIn", context.Principal.Identity.Name);
                               return Task.CompletedTask;
                           },
                           OnSigningOut = context =>
                           {
                               Console.WriteLine("{0} - {1}: {2}", DateTime.Now,
                                 "OnSigningOut", context.HttpContext.User.Identity.Name);
                               return Task.CompletedTask;
                           },
                           OnValidatePrincipal = context =>
                           {
                               Console.WriteLine("{0} - {1}: {2}", DateTime.Now,
                                 "OnValidatePrincipal", context.Principal.Identity.Name);
                               return Task.CompletedTask;
                           }
                       };
                       //options.ExpireTimeSpan = TimeSpan.FromMinutes(10);
                       options.LoginPath = new PathString("/auth/login");
                       options.ReturnUrlParameter = "RequestPath";
                       options.SlidingExpiration = true;
                   })
                   .AddFacebook(options =>
                   {
                       options.AppId = "375096313239989";
                       options.AppSecret = "06320ee53d31fd4a8776092d46593545";

                       //options.Scope.Add("user_birthday");
                       options.Scope.Add("public_profile");

                       options.Fields.Add("birthday");
                       options.Fields.Add("picture");
                       options.Fields.Add("name");
                       options.Fields.Add("email");
                       options.Fields.Add("gender");

                       options.Events = new OAuthEvents
                       {
                           OnCreatingTicket = context =>
                           {
                               var identity = (ClaimsIdentity)context.Principal.Identity;
                               var profileImg = context.User["picture"]["data"].Value<string>("url");
                               identity.AddClaim(new Claim("picture", profileImg));
                               return Task.CompletedTask;
                           }
                       };

                       options.SignInScheme = "TempCookie";
                   })
                .AddTwitter(options =>
                {
                    options.ConsumerKey = "### INSERT TWITTER COMSUMER KEY HERE ###";
                    options.ConsumerSecret = "### INSERT TWITTER COMSUMER SECRET HERE ###";

                    options.SignInScheme = "TempCookie";
                })
                .AddCookie("TempCookie");

            //Nuget Aracılığı ile Microsoft.EntityFrameworkCore.Proxies ve Pomelo.EntityFrameworkCore.MySql kurulması gerekmektedir.
            //Database bağlantısının kurulduğu yerdir.
            services.AddDbContext<ApplicationDbContext>(option => option.UseLazyLoadingProxies().UseMySql(Configuration.GetConnectionString("DefaultConnection")));


            InjectGlobal.InjectGlobalMethod(services);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ApplicationDbContext appContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Story}/{action=Story}/{id?}");
            });

            ////Database silme işlemi
            //appContext.Database.EnsureDeleted();
            ////Database yeniden olu�turulur ve ilk verilerin eklmesi isternirse kullan�l�r.
            DbInitialize.Initialize(appContext);
        }
    }
}
