﻿using MFA_Blog.Dao;
using MFA_Blog.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MFA_Blog.Service
{
    public interface IUserService
    {
        UserDto AddUser(UserDto user, string password); // UserDto -> Geri Dönüş Değeri, addUser -> Metot İsmi, (UserDto user -> Beklediği Sınıf);
        UserDto UpdateUser(UserDto user, string password); // UserDto -> Geri Dönüş Değeri, addUser -> Metot İsmi, (UserDto user -> Beklediği Sınıf);
        UserDto GetUserById(int userId); // UserDto -> Geri Dönüş Değeri, addUser -> Metot İsmi, (UserDto user -> Beklediği Sınıf);
        List<UserDto> GetUsers();
    }
}
