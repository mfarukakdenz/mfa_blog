﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MFA_Blog.Dao;
using MFA_Blog.Dto;
using MFA_Blog.Models;

namespace MFA_Blog.Service.Imp
{
    public class UserServiceImp : IUserService
    {
        IUserDao _userDao;
        public UserServiceImp(IUserDao userDao)
        {
            _userDao = userDao;
        }

        public UserDto AddUser(UserDto user, string password)
        {
            UserModel userModel = new UserModel()
            {
                Email = user.Email,
                Password = password,
                Name = user.Name,
                Surname = user.Surname,
            };

            _userDao.AddUser(userModel);

            user.UserId = userModel.UserId;

            return user;
        }

        public UserDto GetUserById(int userId)
        {
            UserModel userModel = _userDao.GetUserById(userId);

            UserDto userDto = new UserDto() //Her bir modeli Dto ya dönüştürdük
            {
                Email = userModel.Email,
                Name = userModel.Name,
                Surname = userModel.Surname,
                UserId = userModel.UserId
            };

            return userDto;
        }

        public List<UserDto> GetUsers()
        {
            List<UserModel> userModels =  _userDao.GetUsers();
            List<UserDto> userDtos = new List<UserDto>(); // UserDto Collection oluşturduk


            foreach (UserModel userModel in userModels)
            {
                UserDto userDto = new UserDto() //Her bir modeli Dto ya dönüştürdük
                {
                    Email = userModel.Email,
                    Name = userModel.Name,
                    Surname = userModel.Surname,
                    UserId = userModel.UserId
                };

                userDtos.Add(userDto); // Her bir dtoyu ön yüze liste olarak döndürebilmemiz için ilgili 'List<UserDto> userDtos' collection'a ekleme yaptık.
            }

            return userDtos; //Oluşturduğumuz DTO collection'ı geri döndürdük.
        }

        public UserDto UpdateUser(UserDto user, string password)
        {
            UserModel userModel = new UserModel()
            {
                Email = user.Email,
                Password = password,
                Name = user.Name,
                Surname = user.Surname,
                UserId = user.UserId
            };

            _userDao.UpdateUser(userModel);

            return user;
        }
    }
}
